/*global Todos, Ember */
(function () {
    'use strict';
    
    Todos.TodoController = Ember.ObjectController.extend({
        
        // -----------------------------------------------
        // Actions
        // -----------------------------------------------
        actions: {
            editTodo: function () {
                this.set('isEditing', true);
            },
            acceptChanges: function () {
                this.set('isEditing', false);
                if (Ember.isEmpty(this.get('model.title'))) {
                    this.send('removeTodo');
                } else {
                    this.get('model').save();
                }
            },
            removeTodo: function () {
                var todo = this.get('model');
                todo.deleteRecord();
                todo.save();
            }
        },
        
        // -----------------------------------------------
        // Properties
        // -----------------------------------------------
        isEditing: false,
        
        // -----------------------------------------------
        // Computed Properties
        // -----------------------------------------------
        isCompleted: function (key, value) {
            var model = this.get('model');
            if (value === undefined) {
                return model.get('isCompleted');
            }
            
            model.set('isCompleted', value);
            model.save();
            return value;
        }.property('model.isCompleted')
    });
}());