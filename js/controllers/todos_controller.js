/*global Todos, Ember */
(function () {
    'use strict';
    
    Todos.TodosController = Ember.ArrayController.extend({
        actions: {
            createTodo: function () {
                var title = this.get('newTitle'),
                    todo;
                
                if (!title.trim()) {
                    return;
                }
                
                // create the new todo model
                todo = this.store.createRecord('todo', {
                    title: title,
                    isCompleted: false
                });
                
                // clear out the new title field
                this.set('newTitle', '');
                
                todo.save();
            }
        }
    });
    
}());